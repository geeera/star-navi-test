import React, { FC } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { v4 } from "uuid";

import { RootType } from "../../../store/store";
import "./InfoList.scss";

const InfoList: FC = () => {
    const { tiles } = useSelector(({ tile }: RootType) => tile, shallowEqual)

    return (
        <div className="info">
            <h1 className="info--title">Hover squares:</h1>
            <div className="info__list">
                {tiles.map(item =>
                    <p key={v4()} className="info__list--item">row {item.row} col {item.col}</p>
                )}
            </div>
        </div>
    )
}

export default InfoList
