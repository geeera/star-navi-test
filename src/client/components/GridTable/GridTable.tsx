import React, { FC } from "react";
import { v4 } from "uuid";
import { ITile } from "../../../store/reducers/tileReducer/tileReducer";
import "./GridTable.scss";
import { shallowEqual, useSelector } from "react-redux";
import { RootType } from "../../../store/store";


interface IGridTableProps {
    fieldSquare: number,
    onMouseEnter: (tile: ITile) => any
}

const GridTable: FC<IGridTableProps> = (props) => {
    const { fieldSquare, onMouseEnter } = props
    const { tiles } = useSelector(({ tile }: RootType) => tile, shallowEqual)
    let counterRow = 0;
    const fieldSquareArray = Array(fieldSquare**2)
        .fill({})
        .map((item, idx, arr) => {
            const col = (idx % fieldSquare) + 1
            if (!(col - 1)) {
                counterRow++
            }
            return {
                id: idx + 10,
                row: counterRow,
                col
            }
        })
    return (
        <div
            className="grid"
            style={{ gridTemplateColumns: `repeat(${fieldSquare}, 1fr)` }}
        >
            {fieldSquareArray.map((item) =>{
                const isTile = tiles.find(elem => elem.id === item.id)
                const hoverClass = isTile ? 'hover' : ''
                return <span
                    key={v4()}
                    className={"grid--item " + hoverClass}
                    onMouseEnter={() => {
                        onMouseEnter(item)
                    }}
                />
            })}
        </div>
    )
}

export default GridTable
