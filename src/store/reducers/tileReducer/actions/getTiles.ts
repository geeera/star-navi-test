import { Dispatch } from "redux";
import axios from "axios";
import { ITileAction, TileTypes } from "../tileReducer";

export const getTiles = () => async (dispatch: Dispatch<ITileAction>) => {
    try {
        const { data } = await axios.get('https://demo1030918.mockable.io/')
        let arr = [];
        for (const elem in data) {
            const obj = {
                name: `${elem.replace('Mode', '')} Mode`,
                field: data[elem].field
            }
            arr.push(obj)
        }
        // console.log(arr)
        dispatch({
            type: TileTypes.GET_MODES,
            payload: arr
        })
    } catch (e) {

    }
}
