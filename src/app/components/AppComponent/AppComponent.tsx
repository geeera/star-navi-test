import React from 'react';

import MainPage from "../../../client/pages/MainPage";
import { StoreProvider } from '../../../store/store';
import './AppComponent.scss';

const AppComponent = () => {

    return (
        <StoreProvider>
            <div className="App">
                <MainPage />
            </div>
        </StoreProvider>
    );
}

export default AppComponent;
