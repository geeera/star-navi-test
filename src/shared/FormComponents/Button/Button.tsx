import React, { ButtonHTMLAttributes, FC } from "react";

import "./Button.scss";

interface IButtonProps extends ButtonHTMLAttributes<any> {
    value: string
}

const Button: FC<IButtonProps> = ({value, ...rest}) => {

    return <button className="btn" {...rest}>{value}</button>
}

export default Button
