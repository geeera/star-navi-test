import { useCallback, useEffect, useState } from "react";

export const useVisible = (show: boolean, ref: any) => {
    const [isVisible, setIsVisible] = useState(show)
    const handleClickOutside = useCallback((event: any) => {
        if (ref.current && !ref.current.contains(event.target)) {
            setIsVisible(false);
        }
    }, [ref, setIsVisible]);

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    }, [handleClickOutside]);

    return { ref, isVisible, setIsVisible };
}
